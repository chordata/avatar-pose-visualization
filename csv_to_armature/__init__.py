# Copyright 2022 Chordata Tech SL

# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom 
# the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
# OTHER DEALINGS IN THE SOFTWARE.

import bpy
from mathutils import Quaternion, Euler, Vector, Matrix
from time import sleep
from math import pi

from . import operations
import imp
imp.reload(operations)

bpy.types.Object.raw_sensor_vec = bpy.props.FloatVectorProperty(subtype='XYZ')

bpy.types.Object.converted_raw_vec_mag = bpy.props.FloatProperty()

bpy.types.PoseBone.calculated_global_quat_from_chordata = bpy.props.FloatVectorProperty(size = 4, subtype='QUATERNION')
bpy.types.PoseBone.raw_global_quat_from_chordata = bpy.props.FloatVectorProperty(size = 4, subtype='QUATERNION')
bpy.types.PoseBone.calibrated_global_quat_from_chordata = bpy.props.FloatVectorProperty(size = 4, subtype='QUATERNION')


bpy.types.Object.csv_msec = bpy.props.IntProperty()

CSV_FMT_STR = '{time(msec)},{node_label},{q_w},{q_x},{q_y},{q_z},{g_x},{g_y},{g_z},{a_x},{a_y},{a_z},{m_x},{m_y},{m_z}\n'
CSV_HEADER = 'time(msec),node_label,q_w,q_x,q_y,q_z,g_x,g_y,g_z,a_x,a_y,a_z,m_x,m_y,m_z\n'

skip = 1

class CSV_dump_manager:
	def __init__(self, blend_ob, prefix, calibration, inherit_rot = True):
		self.last_time = 0
		self.current_frame = 0
		self.blend_ob = blend_ob
		self.blend_ob.animation_data_clear()
		self.w = prefix + 'w'
		self.x = prefix + 'x'
		self.y = prefix + 'y'
		self.z = prefix + 'z'
		self.calibration = calibration
		# if calibration:
		# 	self.calibration = operations.map_calibration_to_JCS(calibration)
		self.current_pose = {}
		self.inherit_rot = inherit_rot
  
		self.helpers_reset = []

		self.velocities = {}

		self.scene_fps = bpy.context.scene.render.fps
		self.scene_frame_millis = 1000 / self.scene_fps

		blend_ob.animation_data_clear()
		dbones = blend_ob.data.bones
		for b in self.blend_ob.pose.bones:
			b.chordata.Q_local_conjugated = dbones[b.name].matrix.to_quaternion().conjugated()
			b.chordata.Q_local_instant_conj = 1,0,0,0
			b.rotation_quaternion = 1,0,0,0
   
	def get_arrow(self, ob):
		if not getattr(self, "_arrow_cache", None):
			self._arrow_cache = {}

		if ob.name in self._arrow_cache.keys():
			return self._arrow_cache[ob.name]

		if ob.constraints and ob.constraints['Copy Location']:
			arm_target = ob.constraints['Copy Location'].target
			
			for sc_ob in bpy.data.objects:
				if sc_ob.empty_display_type == 'SINGLE_ARROW':
					if sc_ob.constraints and sc_ob.constraints['Copy Location']:
						arrow_target = sc_ob.constraints['Copy Location'].target
						if arrow_target in (arm_target, ob):
							print("--> Found Vector helper for ", ob.name)
							self._arrow_cache[ob.name] = sc_ob
							return sc_ob
	
		return None
	
	def set_on_helper(self, row, ob, label, add_vec = None):
		if self.current_frame % skip != 0: return
		if row['node_label'] == label:
			arrow  = self.get_arrow(ob)
			if label not in self.helpers_reset:
				ob.animation_data_clear()
				if arrow:
					arrow.animation_data_clear()
				self.helpers_reset.append(label)
				print("Clear anim on Helper:", label, ob.name)

			bone = self.blend_ob.pose.bones[label]
			csv_quat = bone.chordata.Q_temp_received
			
			if self.calibration:
				new_quat = operations.apply_calib_to_quat(self.calibration, bone)
			else:
				new_quat = csv_quat
	  
			ob.rotation_quaternion = new_quat
			key_insert = ob.keyframe_insert(\
					'rotation_quaternion',
					index=-1,
					frame=self.current_frame,
					group='rotation')
   
			if add_vec:
				if arrow:
					arrow.empty_display_size = 0.5
					if add_vec == "GYRO":
						DISPLAY_FACTOR = 1
						g_s = 0.07
						vec = Vector((float(row['g_x']), float(row['g_y']), float(row['g_z'])))
						arrow.raw_sensor_vec = vec
						# vec.rotate(csv_quat )
						# vec.rotate(self.aux_calib['pre'][label]) 
						arrow.empty_display_size = vec.magnitude * g_s * DISPLAY_FACTOR
						arrow.converted_raw_vec_mag = vec.magnitude * m_s
      
					elif add_vec in ("ACCEL", "ACCEL_NO_G", "VEL"):
				  			
						DISPLAY_FACTOR = 1
						a_s = 0.000244
						vec = Vector((float(row['a_x']), float(row['a_y']), float(row['a_z'])))
						arrow.raw_sensor_vec = vec
						# vec.rotate(csv_quat )
						# vec.rotate(self.aux_calib['pre'][label])
					
						if add_vec in ( "ACCEL_NO_G", "VEL"):
							gravity = Vector((0,0, - 1 / a_s))
							vec -= gravity
							vec *= -1

							if add_vec == "VEL":
								DISPLAY_FACTOR = 10
								if label not in self.velocities.keys():
									self.velocities[label] = Vector()
								self.velocities[label] += vec
								vec = self.velocities[label] * (1/50)
								
	  
						arrow.empty_display_size = vec.magnitude * a_s * DISPLAY_FACTOR
						arrow.converted_raw_vec_mag = vec.magnitude * a_s

					elif add_vec == "MAG":
						DISPLAY_FACTOR = 1
						m_s = 0.00014
						vec = Vector((float(row['m_x']), float(row['m_y']), float(row['m_z'])))
						arrow.raw_sensor_vec = vec
						# vec.rotate(csv_quat )
						# vec.rotate(self.aux_calib['pre'][label])
						arrow.empty_display_size = vec.magnitude * m_s * DISPLAY_FACTOR
						arrow.converted_raw_vec_mag = vec.magnitude * m_s
      
					else:
						raise TypeError("The add_vec parameter should be one of 'GYRO', 'ACCEL' or 'MAG'")
  
					arrow.rotation_mode = 'QUATERNION'

					world_vec_rotation = vec.to_track_quat('Z','Y')# @ csv_quat.inverted()
					arrow.rotation_quaternion = world_vec_rotation
					
 
					key_insert = arrow.keyframe_insert(\
						'rotation_quaternion',
						index=-1,
						frame=self.current_frame,
						group='rotation')
	 
					key_insert = arrow.keyframe_insert(\
						'empty_display_size',
						index=-1,
						frame=self.current_frame,
						group='display')

					key_insert = arrow.keyframe_insert(\
						'raw_sensor_vec',
						index=-1,
						frame=self.current_frame,
						group='raw_data')
					
					key_insert = arrow.keyframe_insert(\
						'converted_raw_vec_mag',
						index=-1,
						frame=self.current_frame,
						group='raw_data')

			# print("Set key on Helper:", label, ob.name, self.current_frame)
	
	def write_vecs_to_csv(self, row, text_file):
		label = row['node_label']
		bone = self.blend_ob.pose.bones[label]
		g_vec = Vector((float(row['g_x']), float(row['g_y']), float(row['g_z'])))
		a_vec = Vector((float(row['a_x']), float(row['a_y']), float(row['a_z'])))
		m_vec = Vector((float(row['m_x']), float(row['m_y']), float(row['m_z'])))

		csv_quat = bone.chordata.Q_temp_received
		g_vec.rotate(csv_quat )
		g_vec.rotate(self.aux_calib['pre'][label])
		a_vec.rotate(csv_quat )
		a_vec.rotate(self.aux_calib['pre'][label])
		m_vec.rotate(csv_quat )
		m_vec.rotate(self.aux_calib['pre'][label])
		
		mod_row = row.copy()
  
		mod_row['g_x'] = g_vec.x; mod_row['g_y'] = g_vec.y; mod_row['g_z'] = g_vec.z
		mod_row['a_x'] = a_vec.x; mod_row['a_y'] = a_vec.y; mod_row['a_z'] = a_vec.z
		mod_row['m_x'] = m_vec.x; mod_row['m_y'] = m_vec.y; mod_row['m_z'] = m_vec.z
  	
		if not getattr(self, "_csv_header_written", None):
			text_file.clear()
			text_file.write(CSV_HEADER)
			self._csv_header_written = True
	
		text_file.write(CSV_FMT_STR.format(**mod_row))
	
	def insert_timestamp_keyframe(self, frame, timestamp):
		self.blend_ob.csv_msec = timestamp
		self.blend_ob.keyframe_insert(\
			"csv_msec",
			index=-1, 
			frame=frame, 
			group="csv_msec")
 
	def process_row(self, row, set_pose_keyframes = True):
		global skip

		self.current_msec = int(row["time(msec)"])
		if not getattr(self, "initial_timestamp", None):
			self.initial_timestamp = self.current_msec

		if self.current_msec != self.last_time:
			if set_pose_keyframes:
				self.set_keyframes()
			self.current_pose = {}
			self.advance_frame(self.current_msec)

		if self.current_frame % skip == 0:
			self.insert_timestamp_keyframe(self.current_frame, self.current_msec)
			self.store_quat(row)

		self.last_time = self.current_msec

	def store_quat(self, row):
		csv_quat = Quaternion((float(row[self.w]), float(row[self.x]), float(row[self.y]), float(row[self.z]))) 
		bone = self.blend_ob.pose.bones[row['node_label']]
		bone.chordata.Q_temp_received = csv_quat
		bone.chordata.dirty = True 

	def set_keyframes(self):
		self.process_bone(self.blend_ob.pose.bones['base'])

   
	def process_bone(self, bone):
		self.set_keyframe(bone)
		for ch in bone.children:
			self.process_bone(ch)

	def set_keyframe(self, bone):
		#evaluate the transforms before calculating the local
		#bone orientation from the incoming (world) one.
		#this is expensive but ensures Object.convert_space works
		#properly
		depsgraph = bpy.context.evaluated_depsgraph_get()
		object_eval = self.blend_ob.evaluated_get(depsgraph)
		bone_eval = object_eval.pose.bones[bone.name]
  
		def _actually_set_keyframe(bone_name, the_property):
			self.blend_ob.keyframe_insert(\
				'pose.bones["{}"].{}'.format(bone_name, the_property),
				index=-1, 
				frame=self.current_frame, 
				group=bone_name)
  
		if bone.chordata.dirty:
			if self.calibration:
				new_quat = operations.apply_calib_to_quat(self.calibration, bone)
			else:
				new_quat = bone.chordata.Q_temp_received
  
			M_new_quat = new_quat.to_matrix().to_4x4() #remove translation and scale
			bone_position = bone_eval.matrix.to_translation()
			M_new_quat[0][3] = bone_position[0]
			M_new_quat[1][3] = bone_position[1]
			M_new_quat[2][3] = bone_position[2]

			bone.matrix = M_new_quat
			bone_eval.matrix = M_new_quat

			bone.calculated_global_quat_from_chordata = M_new_quat.to_quaternion()
			bone.calibrated_global_quat_from_chordata = new_quat
			bone.raw_global_quat_from_chordata = bone.chordata.Q_temp_received

			_actually_set_keyframe(bone.name, "calculated_global_quat_from_chordata")
			_actually_set_keyframe(bone.name, "calibrated_global_quat_from_chordata")
			_actually_set_keyframe(bone.name, "raw_global_quat_from_chordata")
			_actually_set_keyframe(bone.name, "rotation_quaternion")

			bone.chordata.dirty = False

   
	def advance_frame(self, msec):
		
		ellapsed_msec = msec - self.initial_timestamp 
		new_frame = ellapsed_msec / self.scene_frame_millis

		self.current_frame = int(new_frame)
		bpy.context.scene.frame_end = self.current_frame 

		# print("Advance frame: {:6d} | delta_millis: {:6.0f} , scene_millis {:6.0f}".format( self.current_frame,
					# ellapsed_msec, self.scene_frame_millis * self.current_frame))

	def set_root_translation(self, v, bone_name='base'):
		if self.current_frame % skip != 0: return
		M_trans = Matrix().Translation(v)
		if not getattr(self, "start_M", None):
			self.start_M = self.blend_ob.pose.bones[bone_name].matrix
			self.start_trans = Matrix().Translation(self.start_M.to_translation())

		self.blend_ob.pose.bones[bone_name].matrix = M_trans @ self.start_trans

		self.blend_ob.keyframe_insert(\
				'pose.bones["{}"].location'.format(bone_name),
				index=-1, 
				frame=self.current_frame, 
				group=bone_name)

		try:
			helper = bpy.data.objects['helper_root_motion']
			helper.location = Vector(v) + self.start_M.to_translation() 
			helper.keyframe_insert(\
				'location',
				index=-1, 
				frame=self.current_frame, 
				group='location')
		except KeyError:
			pass