# Copyright 2022 Chordata Tech SL

# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom 
# the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
# OTHER DEALINGS IN THE SOFTWARE.

import bpy
from mathutils import Quaternion
import csv
from os import path
from math import isclose

import imp

import csv_to_armature
imp.reload(csv_to_armature)
from csv_to_armature import CSV_dump_manager
from csv_to_armature import operations

import visualization_parameters as param
imp.reload(param)

# ==================================================================================== #
# This script runs inside Blender to apply an mocap take, recorded as CSV
# together with a calibration calculated by the Chordata Calibrator class
#
# In order to execute it first input the required data in the file
# visualization_parameters.py
#
# Then run the script from the file `avatar_pose_visualization.blend`.
# The blender interface will freeze while the script process the capture,
# in order to see the progress you should look at the system console.
# ==================================================================================== #


# Load user parameters:
# ==================================================================================== #

DATA_CSV = path.join(param.data_csv_folder,"{}.csv".format(param.csv_name))
print(">> Using {:.<18s}: {}".format("DATA_CSV",DATA_CSV))

if path.isabs(DATA_CSV):
    data_file = DATA_CSV
else:
    data_file = path.join(path.dirname(bpy.data.filepath), DATA_CSV)

ROW_RANGE = [param.frame_range[0]*param.sensor_number, param.frame_range[1]*param.sensor_number]
SKIP_FRAMES = max(1,param.frames_to_skip) #<-- avoid division by 0
csv_to_armature.skip = SKIP_FRAMES

if param.calibrator_csv_folder:
	calib_quats = operations.load_calibration_from_csv(param.calibrator_csv_folder, param.take)
else:
	calib_quats = None

translations = operations.get_translations_dict(param.data_csv_folder, param.traj_csv)

new_manager = CSV_dump_manager(bpy.data.objects['JCS_new_calib'], "q_", calib_quats)
old_manager = CSV_dump_manager(bpy.data.objects['JCS_old_calib'], "q_cal_", None)

operations.ensure_use_inherit_rotation(bpy.data.objects['JCS_new_calib'])
operations.ensure_use_inherit_rotation(bpy.data.objects['JCS_old_calib'])

with open(data_file) as csvfile:
	reader = csv.DictReader(csvfile)
	print(reader.fieldnames, "\n", "*"*30)
	
	counter = -1
	found_translations = 0
	for row in reader:
		counter += 1
		if counter < ROW_RANGE[0]: continue
		
		try:
			new_manager.process_row(row, True)

			if param.show_old_calib:
				old_manager.process_row(row, True)

			show_vec = param.add_vec_method if param.show_vectors else None
			new_manager.set_on_helper(row, 
					bpy.data.objects['helper_{}-calib'.format(row['node_label'])], 
					row['node_label'],
					add_vec = show_vec )
			
			if row["time(msec)"] in translations:
				found_translations += 1

				new_manager.set_root_translation(translations[row["time(msec)"]])

		except KeyError as e:
			print("KeyError:", e)
		
		prog_counter = counter - ROW_RANGE[0]
		prog_max_rows = ROW_RANGE[1] - ROW_RANGE[0]
		
		if counter % 50 == 0: #<-- only print one every 50 samples
			translation_str = "| root motion: NOT FOUND"
			if found_translations:
				translation_str = "| root motion: {}/50".format(found_translations)
			print("t:{:6d} | f:{:4d} == progress : {:6.0f}/{:.0f} {:2.2f}% {}".format(\
													int(row["time(msec)"]),
													new_manager.current_frame,
													counter/param.sensor_number, 
													ROW_RANGE[1]/param.sensor_number, 
													(prog_counter/prog_max_rows*100),
													translation_str))
			found_translations = 0
		if counter >= ROW_RANGE[1]: break


print("========= END ===========", param.take)
