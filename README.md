# Avatar pose visualization for Blender

The main script runs inside Blender to apply an mocap take, recorded as CSV
together with a calibration calculated by the Chordata `Calibrator` class

![avatar_pose_visualization](imgs/avatar_pose_visualization.png)

In order to execute it first input the required data in the file\
```visualization_parameters.py```


Then run the script from the file `avatar_pose_visualization.blend`.\
Press the little "PLAY" button you will find in the header of the "Text Editor" area.\
You will find a small "Text Editor" area at the left of the screen in this file.

![run_in_blender](imgs/run_in_blender.png)


The blender interface will freeze while the script process the capture,
in order to see the progress you should look at the system console.

If you don't know how to display the system console in your system take a look [here](https://docs.blender.org/manual/en/2.79/advanced/command_line/introduction.html)