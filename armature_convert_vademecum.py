# Copyright 2022 Chordata Tech SL

# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom 
# the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
# OTHER DEALINGS IN THE SOFTWARE.

import bpy
from mathutils import Matrix, Quaternion
from math import isclose

D = bpy.data
C = bpy.context

arm_ob = C.object
if arm_ob.type != 'ARMATURE':
    raise AssertionError("The active object should be of type 'ARMATURE' to run this script")
ob_mode = arm_ob.mode
bpy.ops.object.mode_set(mode = 'OBJECT')

target_ob = D.objects['target_base']
e1 = D.objects['Empty_1']
e2 = D.objects['Empty_2']

which_bone = 'l-hand'
data_bone = arm_ob.data.bones[which_bone]
pose_bone = arm_ob.pose.bones[which_bone]

try:
    print("========== Transforms converts of bone: {} ==========\n".format(data_bone.name))

    def assert_M(ma, mb, max_dim=4, tolerance = 1e-10):
        '''Compare two matrices with tolerance'''
        for i, vec in enumerate(ma):
            if i == max_dim : break
            for j, val in enumerate(vec):
                if j == max_dim : break
                if not isclose(val, mb[i][j], abs_tol = tolerance ):
                    raise AssertionError(" Matrix diff in [{}:{}]\nValues: {:.16f} | {:.16f}\nTolerance: {}\n{}\n{}".format(
                        i,j,val, mb[i][j], tolerance, ma, mb))
                    

    # === Get the (data)Bone rotation in:
    M_bone = data_bone.matrix # bone space (relative to parent bone)
    M_local = data_bone.matrix_local # local space (relative to armature) 

    #======================================================
    #=== Calculate Bone local_space and bone_space Matrix 
    #======================================================

    # === Calculate bone space matrix from local space
    if data_bone.parent:
        M_bone_calc = data_bone.parent.matrix_local.inverted() @ M_local 
    else:
        M_bone_calc = Matrix.Identity(4) @ M_local 

    M_bone_calc = M_bone_calc.to_3x3()

    assert_M(M_bone_calc, M_bone)
    print( "M_BONE:\n", M_bone)
    print( "M_BONE_CALC:\n", M_bone_calc)

    # === Calculate local space matrix from bone space
    if data_bone.parent:
        M_local_calc = Matrix.Translation(data_bone.parent.tail_local - data_bone.parent.head_local) \
                        @ data_bone.parent.matrix_local \
                        @ Matrix.Translation(data_bone.head) \
                        @ M_bone.to_4x4() 
    else:
        M_local_calc = Matrix.Translation(data_bone.head) @ M_bone.to_4x4() 

    assert_M(M_local_calc, M_local, 4)
    print( "M_LOCAL:\n", M_local)
    print( "M_LOCAL_CALC:\n", M_local_calc)

    #================================================
    #=== Compare Bone and PoseBone spaces and props
    #================================================

    # === PoseBone.matrix vs Bone.matrix_local
    arm_ob.data.pose_position = 'REST'
    bpy.ops.object.mode_set(toggle=True) #<-- (otherwise the pose_position is not applied)
    
    # They are equal when in rest pose, or with no pose transforms 
    assert_M(data_bone.matrix_local, pose_bone.matrix)
    
    # === Obtain PoseBone.matrix with Bone.matrix
    arm_ob.data.pose_position = 'POSE'
    bpy.ops.object.mode_set(toggle=True) #<--  (otherwise the pose_position is not applied)
        
    bone_vec = pose_bone.head - pose_bone.parent.head
    M_pose_calc = Matrix.Translation(bone_vec) \
                    @ pose_bone.parent.matrix \
                    @ data_bone.matrix.to_4x4() \
                    @ pose_bone.matrix_basis

    assert_M(M_pose_calc, pose_bone.matrix, tolerance = 0.1)
    
    #================================================
    #=== Using Object.convert_space
    #================================================
    
    # === Assign a world rotation
    
    M_target = target_ob.matrix_world
    M_pose_convert = arm_ob.convert_space(pose_bone = pose_bone, matrix = M_target, from_space = 'WORLD', to_space = 'LOCAL')
    
    M_pose_convert_rot = M_pose_convert.to_quaternion().to_matrix().to_4x4() #remove translation and scale
    pose_bone.matrix_basis = M_pose_convert_rot
    
    # === Get a PoseBone rotation in pose space

    M_pbone_pose = arm_ob.convert_space( pose_bone = pose_bone, matrix = pose_bone.matrix_basis, from_space = 'LOCAL', to_space = 'POSE')

    e1.matrix_world = M_pbone_pose
    
finally:
    bpy.ops.object.mode_set(mode = ob_mode)