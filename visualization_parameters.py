# Copyright 2022 Chordata Tech SL

# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom 
# the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
# OTHER DEALINGS IN THE SOFTWARE.


# ==================================================================================== #
# The main script runs inside Blender to apply an mocap take, recorded as CSV
# together with a calibration calculated by the Chordata Calibrator class
#
# In order to execute it first input the required data described in steps 1 to 6 below.

# Then run the script from the file `avatar_pose_visualization.blend`.
# The blender interface will freeze while the script process the capture,
# in order to see the progress you should look at the system console.
# ==================================================================================== #

# ==================================================================================== #
# User parameters:
# ==================================================================================== #

# ~~~~~~~~~~~~~~~ CSV Files ~~~~~~~~~~~~~~~ #
#1.	Enter the name of the take, it should be the same than the one used 
#	as argument to the method Calibrator.save_calib_quats().
#	The script will build the correct filenames according to 
#	the convention used by the Calibrator.
take = "calibrated_Tecnocampus_masha_06"

#2. Enter the folder where the Calibrator saved the CSV files. 
#   Set to None to omit calibration and use raw data
calibrator_csv_folder = None

#3. Enter the data csv folder, where the original takes were recorded and saved as CSV
# data_csv_folder = "/home/daylan/Proyectos/105_ChordataTech/framework/ZUPT/iez-ips/mocap_data"
data_csv_folder = "/home/daylan/Proyectos/105_ChordataTech/framework/ZUPT/iez-ips/mocap_data/"


#3.1 	The script will build the data csv filename based in the folder and take name.
#		If you are using a different naming convention, set the csv_name variable
csv_name = take

#3.2 	The script will build the trajectory csv filename based in the folder and take name.
#		If you are using a different naming convention, set the traj_name variable
traj_csv = "trajectory_" 

# ~~~~~~~~~~~~~~~ Range to process ~~~~~~~~~~~~~~~ #
# This script requires some time to be executed, the parameters below allows
# to restrict the processing range, obtaining quick previews or complete sequences

#4 Enter the frame range you wish to visualize
frame_range = (500, 2500)

#4.1 	The script will calculate the row range to process based on the number of sensors used.
#		The sensor numer is normally 15, but you can change it below
sensor_number = 15

#5. You can skip some frames in obtain a quicker preview
frames_to_skip = 8

#6. You can visualize vectors as arrows (defaults to False)
show_vectors = True

#6.1 	Select the vector to be displayed, can be: ("ACCEL", "ACCEL_NO_G", "VEL", "MAG")
# Note: the vector visualization is WIP, if you find any inconsitency take a look at 
# csv_to_armature.CSV_dump_manager.set_on_helper() method
add_vec_method = "ACCEL_NO_G"

#7 Optional parameters
show_old_calib = False


# ==================================================================================== #
# End user parameters
# ==================================================================================== #
