# Copyright 2022 Chordata Tech SL

# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom 
# the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included 
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
# OTHER DEALINGS IN THE SOFTWARE.

from mathutils import Matrix, Quaternion, Euler, Vector
from math import pi
from os import path
import csv
import bpy
from math import isclose
import mathutils
# Note the math objects using here come from the 'mathutils'
# internal Blender module.
# Documentation can be found at:
# https://docs.blender.org/api/2.82a/mathutils.html

# def map_calibration_to_JCS(calibration_dict):

#     new_calib = {"post":{}, "pre":{}}

#     new_calib["post"] = calibration_dict["post"] 
#     new_calib["pre"] = calibration_dict["pre"]

#     return new_calib


def apply_calib_to_quat(calibration, bone):
    '''
    This function applies the calibration to an incoming raw quaternion from a sensor_fused KCeptor.
    The incoming quat was previously stored in the bone property
    bone.chordata.Q_temp_received
    '''

    post_quat = calibration["post"][bone.name]
    pre_quat = calibration["pre"][bone.name]

    new_quat = pre_quat @ bone.chordata.Q_temp_received @ post_quat

    return new_quat

def load_calibration_from_csv(calibrator_csv_folder, take):
    POST_CALIB_CSV = path.join(calibrator_csv_folder,"rotation_pos_{}.csv".format(take))
    PRE_CALIB_CSV =  path.join(calibrator_csv_folder,"rotation_pre_{}.csv".format(take))
    print(">> Using {:.<18s}: {}".format("PRE_CALIB_CSV",PRE_CALIB_CSV))
    print(">> Using {:.<18s}: {}".format("POST_CALIB_CSV",POST_CALIB_CSV))

    calib_quats = {"pre": {}, "post": {}}

    if path.isabs(POST_CALIB_CSV):
        calib_file = POST_CALIB_CSV
    else:
        calib_file = path.join(path.dirname(bpy.data.filepath), POST_CALIB_CSV)


    with open(calib_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            q_values = (float(row['w']), float(row['x']), float(row['y']), float(row['z']))
            calib_quats["post"][row['node_label']] = Quaternion(q_values)
            print("QUAT magnitude", row['node_label'], calib_quats['post'][row['node_label']].magnitude)
            assert isclose(calib_quats['post'][row['node_label']].magnitude, 1, rel_tol=1e-5)

    if path.isabs(PRE_CALIB_CSV):
        calib_file = PRE_CALIB_CSV
    else:
        calib_file = path.join(path.dirname(bpy.data.filepath), PRE_CALIB_CSV)

    with open(calib_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            q_values = (float(row['w']), float(row['x']), float(row['y']), float(row['z']))
            calib_quats["pre"][row['node_label']] = Quaternion(q_values)
            print("QUAT magnitude", row['node_label'], calib_quats['pre'][row['node_label']].magnitude)
            assert isclose(calib_quats['pre'][row['node_label']].magnitude, 1, rel_tol=1e-2)

    return calib_quats


def get_translations_dict(data_csv_folder, traj_csv):
    TRANSLATION_CSV = path.join(data_csv_folder,"{}.csv".format(traj_csv))
    print(">> Using {:.<18s}: {}".format("TRANSLATION_CSV",TRANSLATION_CSV))

    if path.isabs(TRANSLATION_CSV):
        translation_file = TRANSLATION_CSV
    else:
        translation_file = path.join(path.dirname(bpy.data.filepath), TRANSLATION_CSV)

    translations = {}
    count_translation = 0
    try:
        with open(translation_file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                translations[row["time(msec)"]] = (float(row['x']), float(row['y']), float(row['z']))
                if count_translation < 10: print(row["time(msec)"])
                count_translation += 1
    except Exception as e:
        print("Error opening translation file:", e)

    return translations

def ensure_use_inherit_rotation(ob):
    for bone in ob.data.bones:
        bone.use_inherit_rotation = True


